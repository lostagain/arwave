#ifndef ARBLIPLIST_H
#define ARBLIPLIST_H
#include <QVBoxLayout>
#include <QWidget>
 #include <QSlider>
 #include <QListWidget>

//class contains a table for blips, some related functions
//and a range slider at the bottom
class ARBliplist : public QVBoxLayout
{
public:
    ARBliplist();

    QListWidget* getListWidget();
  // QSlider* getRangeSlider();


private:

    QListWidget *listWidget;
   // QSlider *rangeLimit;


};

#endif // ARBLIPLIST_H
