#include "arbliparray.h"
#include "arblip.h"
 #include <QDebug>

/** ARBlip Array handles storing, sorting and general management of ARblips **/

ARBlipArray::ARBlipArray()
{
}
void ARBlipArray::addBlip(ARBlip newblip){


    //Validation of blip goes here
    //

    // if valid


    //does a blip with this ID already exist?
    int indexOfBlip = indexOf(newblip);
    if (indexOfBlip>-1){
        //if it does, then we replace it;
        qDebug()<<"replacing";
        arblipStore.replace(indexOfBlip,newblip);
    }   else {
        // if new we add it to the store;
        arblipStore.append(newblip);
    }






}

/** clear all blips in the store **/
void ARBlipArray::clear(){
    arblipStore.clear();
}


/** returns all the blips in the order they are stored **/
QList<ARBlip> ARBlipArray::getAllBlips(){
    return arblipStore;
}


/** returns all the blips within a range **/
QList<ARBlip> ARBlipArray::getBlipsWithin(location CurrentLocation,double range)
{

    //create temp list;
    QList <ARBlip> subset;

    //add everything within range

    int i = 0;
    for (i=0;i<arblipStore.length();i++)
    {
        if (arblipStore[i].getLoc().distanceTo(CurrentLocation)<range )
        {
            subset.append(arblipStore[i]);
        }

    }

    return subset;
}



// Various sorts of comparisons for ARBlips

bool orderbytimestamp_asc(ARBlip& s1, ARBlip& s2)
{
  QDateTime temp1=s1.getLastUpdateTime();
  QDateTime temp2=s2.getLastUpdateTime();


  return (temp1<temp2);

}
bool orderbytimestamp_dec(ARBlip& s1, ARBlip& s2)
{
  QDateTime temp1=s1.getLastUpdateTime();
  QDateTime temp2=s2.getLastUpdateTime();

  return (temp1>temp2);

}



/** returns a copy of all the blips in order of their timestamps **/
/** boolean controls if its ordered up or down **/

QList<ARBlip> ARBlipArray::getAllBlipsOrderedByTimestamp(bool asc){

    //create temp list;
    QList <ARBlip> orderedarray;

    orderedarray=arblipStore;
     qDebug() << "sorting by timestamp";
     if (asc){
    qSort(orderedarray.begin(),orderedarray.end(),orderbytimestamp_asc);
} else {
     qSort(orderedarray.begin(),orderedarray.end(),orderbytimestamp_dec);
}
    qDebug() << "sorted";

    return orderedarray;



}


/** returns the last blip added **/
ARBlip ARBlipArray::getLastAddedBlip(){
    if (arblipStore.isEmpty()){
        //return empty error
        ARBlip error("Error_1","-",34.34,34.34,313.55,5,5,5,"No blip found error. Arraystore is empty",QDateTime::currentDateTime());
        return error;
    }
    return arblipStore.last();
}

int ARBlipArray::indexOf(ARBlip testagainst)
{

    //if the array is empty, we can exit straight away
    if (arblipStore.isEmpty()){
        return -1;
    }
    //else we loop looking for it
    int i = 0;
    for (i=0;i<arblipStore.length();)
    {
        if (arblipStore[i].getRefID() == testagainst.getRefID())
        {
            return i;
        }
    i++;
    }

    return -1;

}

ARBlip ARBlipArray::at(int index)
{
    return arblipStore.at(index);
}

int ARBlipArray::removeARBlipByID(QByteArray id)
{
    for(int i = 0; i < arblipStore.length(); i++)
    {
        if( arblipStore[i].getRefID() == id )
        {
            arblipStore.removeAt(i);
            return i;
        }
    }
    //default return
    return -1;
}

ARBlip* ARBlipArray::getARBlipById(QByteArray ID)
{
    for(int i = 0; i < arblipStore.length(); i++)
    {
        if( arblipStore[i].getRefID() == ID )
        {
            return &arblipStore[i];
        }
    }
    return NULL;
}
