#ifndef ARBLIPARRAY_H
#define ARBLIPARRAY_H
#include "arblip.h"
#include "location.h"
class ARBlipArray
{
public:
    ARBlipArray();
    void addBlip(ARBlip);
    void clear();

    QList<ARBlip> getAllBlips();
    QList<ARBlip> getAllBlipsOrderedByTimestamp(bool);
    QList<ARBlip> getBlipsWithin(location,double);

    ARBlip getLastAddedBlip();

    int indexOf(ARBlip);
    ARBlip at(int);
    int removeARBlipByID(QByteArray);
    ARBlip* getARBlipById(QByteArray);
private:
    QList<ARBlip> arblipStore;

};

#endif // ARBLIPARRAY_H
