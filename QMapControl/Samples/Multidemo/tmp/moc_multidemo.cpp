/****************************************************************************
** Meta object code from reading C++ file 'multidemo.h'
**
** Created: Sat 16. Jan 13:12:31 2010
**      by: The Qt Meta Object Compiler version 61 (Qt 4.5.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../src/multidemo.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'multidemo.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 61
#error "This file was generated using the moc from 4.5.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Multidemo[] = {

 // content:
       2,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   12, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors

 // signals: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x05,
      21,   10,   10,   10, 0x05,
      31,   10,   10,   10, 0x05,
      40,   10,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
      64,   50,   10,   10, 0x0a,
     103,  101,   10,   10, 0x0a,
     149,  101,   10,   10, 0x0a,
     199,   10,   10,   10, 0x0a,
     219,   10,   10,   10, 0x0a,
     238,   10,   10,   10, 0x0a,
     254,   10,   10,   10, 0x0a,
     274,  101,   10,   10, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Multidemo[] = {
    "Multidemo\0\0setX(int)\0setY(int)\0zoomIn()\0"
    "zoomOut()\0geom,coord_px\0"
    "geometryClickEvent(Geometry*,QPoint)\0"
    ",\0coordinateClicked(const QMouseEvent*,QPointF)\0"
    "coordinateClicked_mc2(const QMouseEvent*,QPointF)\0"
    "buttonToggled(bool)\0toggleFollow(bool)\0"
    "toggleGPS(bool)\0draggedRect(QRectF)\0"
    "mouseEventCoordinate(const QMouseEvent*,QPointF)\0"
};

const QMetaObject Multidemo::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Multidemo,
      qt_meta_data_Multidemo, 0 }
};

const QMetaObject *Multidemo::metaObject() const
{
    return &staticMetaObject;
}

void *Multidemo::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Multidemo))
        return static_cast<void*>(const_cast< Multidemo*>(this));
    return QWidget::qt_metacast(_clname);
}

int Multidemo::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: setX((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: setY((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: zoomIn(); break;
        case 3: zoomOut(); break;
        case 4: geometryClickEvent((*reinterpret_cast< Geometry*(*)>(_a[1])),(*reinterpret_cast< QPoint(*)>(_a[2]))); break;
        case 5: coordinateClicked((*reinterpret_cast< const QMouseEvent*(*)>(_a[1])),(*reinterpret_cast< const QPointF(*)>(_a[2]))); break;
        case 6: coordinateClicked_mc2((*reinterpret_cast< const QMouseEvent*(*)>(_a[1])),(*reinterpret_cast< const QPointF(*)>(_a[2]))); break;
        case 7: buttonToggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: toggleFollow((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: toggleGPS((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: draggedRect((*reinterpret_cast< QRectF(*)>(_a[1]))); break;
        case 11: mouseEventCoordinate((*reinterpret_cast< const QMouseEvent*(*)>(_a[1])),(*reinterpret_cast< const QPointF(*)>(_a[2]))); break;
        default: ;
        }
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void Multidemo::setX(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Multidemo::setY(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Multidemo::zoomIn()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void Multidemo::zoomOut()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}
QT_END_MOC_NAMESPACE
