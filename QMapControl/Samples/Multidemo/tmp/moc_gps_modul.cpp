/****************************************************************************
** Meta object code from reading C++ file 'gps_modul.h'
**
** Created: Sat 16. Jan 13:12:36 2010
**      by: The Qt Meta Object Compiler version 61 (Qt 4.5.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../src/gps_modul.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'gps_modul.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 61
#error "This file was generated using the moc from 4.5.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_GPS_Modul[] = {

 // content:
       2,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   12, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors

 // signals: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x05,
      33,   10,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
      43,   10,   10,   10, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_GPS_Modul[] = {
    "GPS_Modul\0\0new_position(QPointF)\0"
    "changed()\0tick()\0"
};

const QMetaObject GPS_Modul::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_GPS_Modul,
      qt_meta_data_GPS_Modul, 0 }
};

const QMetaObject *GPS_Modul::metaObject() const
{
    return &staticMetaObject;
}

void *GPS_Modul::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_GPS_Modul))
        return static_cast<void*>(const_cast< GPS_Modul*>(this));
    return QObject::qt_metacast(_clname);
}

int GPS_Modul::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: new_position((*reinterpret_cast< QPointF(*)>(_a[1]))); break;
        case 1: changed(); break;
        case 2: tick(); break;
        default: ;
        }
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void GPS_Modul::new_position(QPointF _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void GPS_Modul::changed()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE
