/****************************************************************************
** Meta object code from reading C++ file 'citymap.h'
**
** Created: Sat 16. Jan 13:14:21 2010
**      by: The Qt Meta Object Compiler version 61 (Qt 4.5.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../src/citymap.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'citymap.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 61
#error "This file was generated using the moc from 4.5.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Citymap[] = {

 // content:
       2,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   12, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors

 // slots: signature, parameters, type, tag, flags
      25,    9,    8,    8, 0x0a,
      77,   62,    8,    8, 0x0a,
     111,   62,    8,    8, 0x0a,
     154,    8,    8,    8, 0x0a,
     166,  164,    8,    8, 0x0a,
     204,    8,    8,    8, 0x0a,
     219,  164,    8,    8, 0x0a,
     265,    8,    8,    8, 0x0a,
     306,  295,    8,    8, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Citymap[] = {
    "Citymap\0\0evnt,coordinate\0"
    "hideNote(const QMouseEvent*,QPointF)\0"
    "geometry,point\0geometryClicked(Geometry*,QPoint)\0"
    "geometryClickEventKneipe(Geometry*,QPoint)\0"
    "addNote()\0,\0writeNote(const QMouseEvent*,QPointF)\0"
    "calcDistance()\0"
    "calcDistanceClick(const QMouseEvent*,QPointF)\0"
    "mapproviderSelected(QAction*)\0geom,point\0"
    "editNote(Geometry*,QPoint)\0"
};

const QMetaObject Citymap::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Citymap,
      qt_meta_data_Citymap, 0 }
};

const QMetaObject *Citymap::metaObject() const
{
    return &staticMetaObject;
}

void *Citymap::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Citymap))
        return static_cast<void*>(const_cast< Citymap*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int Citymap::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: hideNote((*reinterpret_cast< const QMouseEvent*(*)>(_a[1])),(*reinterpret_cast< const QPointF(*)>(_a[2]))); break;
        case 1: geometryClicked((*reinterpret_cast< Geometry*(*)>(_a[1])),(*reinterpret_cast< QPoint(*)>(_a[2]))); break;
        case 2: geometryClickEventKneipe((*reinterpret_cast< Geometry*(*)>(_a[1])),(*reinterpret_cast< QPoint(*)>(_a[2]))); break;
        case 3: addNote(); break;
        case 4: writeNote((*reinterpret_cast< const QMouseEvent*(*)>(_a[1])),(*reinterpret_cast< const QPointF(*)>(_a[2]))); break;
        case 5: calcDistance(); break;
        case 6: calcDistanceClick((*reinterpret_cast< const QMouseEvent*(*)>(_a[1])),(*reinterpret_cast< const QPointF(*)>(_a[2]))); break;
        case 7: mapproviderSelected((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 8: editNote((*reinterpret_cast< Geometry*(*)>(_a[1])),(*reinterpret_cast< QPoint(*)>(_a[2]))); break;
        default: ;
        }
        _id -= 9;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
