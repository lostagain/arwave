/****************************************************************************
** Meta object code from reading C++ file 'mapnetwork.h'
**
** Created: Sat 16. Jan 13:10:16 2010
**      by: The Qt Meta Object Compiler version 61 (Qt 4.5.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/mapnetwork.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mapnetwork.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 61
#error "This file was generated using the moc from 4.5.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_qmapcontrol__MapNetwork[] = {

 // content:
       2,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   12, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors

 // slots: signature, parameters, type, tag, flags
      34,   25,   24,   24, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_qmapcontrol__MapNetwork[] = {
    "qmapcontrol::MapNetwork\0\0id,error\0"
    "requestFinished(int,bool)\0"
};

const QMetaObject qmapcontrol::MapNetwork::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_qmapcontrol__MapNetwork,
      qt_meta_data_qmapcontrol__MapNetwork, 0 }
};

const QMetaObject *qmapcontrol::MapNetwork::metaObject() const
{
    return &staticMetaObject;
}

void *qmapcontrol::MapNetwork::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_qmapcontrol__MapNetwork))
        return static_cast<void*>(const_cast< MapNetwork*>(this));
    return QObject::qt_metacast(_clname);
}

int qmapcontrol::MapNetwork::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: requestFinished((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        default: ;
        }
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
