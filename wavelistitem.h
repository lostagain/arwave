#ifndef WAVELISTITEM_H
#define WAVELISTITEM_H

#include <QWidget>
#include <QListWidgetItem>

class WaveListItem : public QListWidgetItem
{
public:
    WaveListItem(QString);
    void updateTimeStamp();
    void setTimeStamp(QDateTime*);
    QString getID();
    QString getWaveName();
private:
    QString wavesName;
    QString waveID;
    QDateTime *wavesLastUpdate; //this would be the last time any blip was updated on the wave
};

#endif // WAVELISTITEM_H
