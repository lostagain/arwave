#include "waveModel.h"

//FIXME: is this needed? and if yes, is this the right way to initialize things up?
WaveModel::WaveModel()
{
    m_id = "TEMP";
    m_lastChange = QDateTime::currentDateTime();
    m_title = "placeholder";
}

WaveModel::WaveModel(QString id, QDateTime lastUpdate, QString title)
{
    m_id = id;
    m_lastChange = lastUpdate;
    m_title = title;
}

WaveModel::~WaveModel()
{
    //???
}

QString WaveModel::id()
{
    return m_id;
}

QDateTime WaveModel::lastChange()
{
    return m_lastChange;
}

void WaveModel::setId(QString id)
{
    m_id = id;
}

QString WaveModel::title()
{
    return m_title;
}

void WaveModel::setTitle(QString title)
{
    m_title = title;
}
