#ifndef LOCATION_H
#define LOCATION_H

class location
{
public:
    location();
    //placement
    double Xpos;
    double Ypos;
    double Zpos;

    //usefull functions
    double distanceToCartesian(location);

    double distanceTo(location);

    double distVincenty(double lat1,double lon1, double lat2, double lon2);


};

#endif // LOCATION_H
