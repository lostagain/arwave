#ifndef WAVECONNECTIONICON_H
#define WAVECONNECTIONICON_H

#include <QWidget>
#include <QLabel>

QT_BEGIN_NAMESPACE
class QLabel;
QT_END_NAMESPACE

class WaveConnectionIcon : public QLabel
{
    Q_OBJECT

public:
    WaveConnectionIcon();
    ~WaveConnectionIcon();
    void nextFrame();
    void start();
    void stop();

public slots:
    void updateFrame();

private:
    int currentframe;
    QList<QPixmap *> animationframes;
    QTimer *timer;
};

#endif // WAVECONNECTIONICON_H
