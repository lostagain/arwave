#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include <QErrorMessage>
#include <QGraphicsTextItem>
#include "waveconnectionicon.h"
#include "ui_mainwindow.h"
#include "QMapControl/qmapcontrol.h"

//stuffs we did
#include "arblip.h"
#include "arbliplistitem.h"
#include "arbliparray.h"
#include "location.h"
#include "arbliplist.h"
#include "abstractCommunicationManager.h"
#include "pyGoCommunicationManager.h"
#include "fedonecommunicationmanager.h"
#include "waveListModel.h"


using namespace qmapcontrol;

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();


    //current application state
    //(not sure if this is the best way to do it)
    enum State {
             NotLoggedIn = 0x0,
             AddingBlip = 0x1,
             EditingBlip = 0x2,
             ViewingBlips = 0x3,
             ViewingWaves = 0x5
         };
    Q_DECLARE_FLAGS(States, State)

    States currentstate;

    void addWave(QString waveID, QDateTime lastChange, QString title);
    WaveListModel *waveListModel;

public slots:
   void selectServer(void);
   void login(void);
   void logout(void);
   void showWaveList(void);
   void showActiveWave(void);
   //void stateHandler(int);
   void createWave(void);
   void openWave(void);
   void inviteUserToWave(void);
   void displayBlipText(QListWidgetItem*);
   void changeRangeLimit();
   void goToMap();
   void gotolocation();
   void setARBlipLocation();
   void zoomMap(int);
   void mapViewChanged(QPointF,int);
   void mapProviderSelected(QAction*);
   void addNewBlipFromMap();
   void editSelectedBlip_clicked();
   void deleteSelectedBlip_clicked();
   void updateARDot(ARBlip blip);
   void cancelSubmit();
   void setClientLocation();


private:
    Ui::MainWindow *ui;
    QList<ImagePoint*> arDots;
    AbstractCommunicationManager *m_acm;
    //TODO
    //this will be obtained by a gps or something in the future
    location currentPosition;
    int lastOpenedPanel;
    WaveConnectionIcon *connectionIcon;
    ARBlipArray arblips;
    //widgets
    ARBliplist *arblipDisplay;
    MapControl* mapControl;
    MapAdapter* mapAdapter;
    //current range limit
    double rangeLimit;
    Layer* layer;
    GeometryLayer* ARLayer;
    QMenu* mapMenu;
    QAction* osmAction;
    QAction* yahooActionMap;
    QAction* yahooActionSatellite;
    QAction* yahooActionOverlay;
    QAction* googleActionMap;

    QSlider* zoomSlider;

    //WaveListModel waveListModel;

    //FixMe?: Maybe this should only store the IDRef for the blip.
    ARBlip currentlyEditing; //stores the blip currently being edited

    void updateARBlipList();
    //void updateARBlip(const PyGoWave::Blip *);
    void createMapMenus();
    void drawARDots();

//FIXME: are this really private slots?
private slots:
    void on_submitNewBlip_clicked();
    void on_postNewBlip_clicked();
    void acm_arblipsUpdated();

};


Q_DECLARE_OPERATORS_FOR_FLAGS(MainWindow::States)

#endif // MAINWINDOW_H
