#ifndef WAVELISTMODEL_H
#define WAVELISTMODEL_H

#include <QAbstractListModel>
#include <QList>
#include "waveModel.h"

class WaveListModel: public QAbstractListModel
{

public:
    WaveListModel(const QList<WaveModel *> &waves, QObject *parent = 0)
             : QAbstractListModel(parent), m_waves(waves) {}

    //TODO: methods not implemented yet
    //return infos to the view such as if the mdoel is editable, sortable, etc...
    Qt::ItemFlags flags( const QModelIndex &index ) const;
    //used to retrieve data
    QVariant data( const QModelIndex &index , int role = Qt::DisplayRole ) const;
    //used to set data
    bool setData ( const QModelIndex & index, const QVariant & value, int role = Qt::EditRole );
    //returns the number of rows
    int rowCount ( const QModelIndex & parent = QModelIndex() ) const;
    //returns some kind of headers with info to display to the user
    QVariant headerData ( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;
    //sets the header data
    bool setHeaderData ( int section, Qt::Orientation orientation, const QVariant & value, int role = Qt::EditRole );
    //insert a new row, should be called when a new wave is added, both by us or by a remote user
    bool insertRows ( int row, int count, const QModelIndex & parent = QModelIndex() );
    //same as above, just removes
    bool removeRows ( int position, int rows, const QModelIndex & parent = QModelIndex() );

    //TODO: signals/slots

    //TODO: vars
private:
    //this is a list of the waves in our own wave model
    QList<WaveModel *> m_waves;

};

Q_DECLARE_METATYPE(WaveModel *)
#endif // WAVELISTMODEL_H
