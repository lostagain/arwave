#include "waveconnectionicon.h"
#include <QtGui>

WaveConnectionIcon::WaveConnectionIcon()
{

    //load all pixal maps into array

    //QPixmap *Frame1 = new QPixmap(":/icons/waveicon1.jpg");

    animationframes.append(new QPixmap(":/icons/waveiconb01.png"));
    animationframes.append(new QPixmap(":/icons/waveiconb02.png"));
    animationframes.append(new QPixmap(":/icons/waveiconb03.png"));
    animationframes.append(new QPixmap(":/icons/waveiconb04.png"));
    currentframe=0;
    this->setPixmap(*animationframes[currentframe]);

    this->show();
    this->setAttribute(Qt::WA_DeleteOnClose);
    currentframe=0;

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateFrame()));


}
WaveConnectionIcon::~WaveConnectionIcon(){

}
void WaveConnectionIcon::updateFrame()
{
    this->nextFrame();
    return;
}
void WaveConnectionIcon::nextFrame()
{

    currentframe++;
    if (currentframe>=animationframes.count()){
    currentframe=0;
    }
    this->setPixmap(*animationframes[currentframe]);
    return;
}

void WaveConnectionIcon::start(){
     timer->setInterval(100);
     timer->start();
}

void WaveConnectionIcon::stop(){
   timer->stop();
}
