#include "arbliplistitem.h"
#include "arblip.h"
#include "location.h"

ARBlipListItem::ARBlipListItem(ARBlip sourceblip, location CurrentLoc)
{
arblip=sourceblip;

//current location is just 0,0,0 at the moment, till we work out how to add gps support

QString distance;
distance.setNum(arblip.getLoc().distanceTo(CurrentLoc));
//(in future we really need to parse a slot to handle auto-updateing of location)

this->setText(this->getBlipsDataAsTextShort()+"     X="+arblip.getXAsString()+" Y="+arblip.getYAsString()+" Z="+arblip.getZAsString()+" Distance="+distance.leftJustified(10, ' ')+" m        ["+arblip.getLastUpdateTime().toString()+"] ");
this->setToolTip("(Blip ID="+arblip.getRefID()+")");


}

QString ARBlipListItem::getBlipsDataAsText(){
    //(should check its inline text data first, and error if its not)
    return arblip.getDataAsString();
}


QString ARBlipListItem::getBlipsDataAsTextShort(){
    //(should check its inline text data first, and error if its not)

    //return exactly 35 charecters, no more, no less
    return (arblip.getDataAsString().left(20)+"...").leftJustified(30, ' ');
}

ARBlip ARBlipListItem::getARBlip()
{
    return arblip;
}

