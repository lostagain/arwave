/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Tue Mar 30 13:11:57 2010
**      by: Qt User Interface Compiler version 4.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QFrame>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QSlider>
#include <QtGui/QStackedWidget>
#include <QtGui/QStatusBar>
#include <QtGui/QTextEdit>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *action_Quit;
    QAction *actionLogout;
    QAction *action_show_list;
    QAction *actionOpen_wave;
    QAction *actionPygowaveConnect;
    QAction *localSeverConnect;
    QAction *action_show_active_wave;
    QAction *testSeverConnect;
    QAction *setClientLocationZero;
    QAction *setClientLocationTilburg;
    QAction *action_current_real_location;
    QAction *setRangeLimit50m;
    QAction *setRangeLimit100m;
    QAction *setRangeLimit200m;
    QAction *setRangeLimit500m;
    QAction *setRangeLimit1km;
    QAction *setRangeLimit5km;
    QAction *setRangeLimit50km;
    QAction *setRangeLimit100km;
    QAction *setRangeLimitInfinite;
    QAction *setClientLocationNewYork;
    QAction *setClientLocationLondon;
    QAction *actionOrient_lodge;
    QWidget *centralWidget;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QLineEdit *usrLine;
    QPushButton *loginButton;
    QLabel *label;
    QLineEdit *passLine;
    QLabel *label_2;
    QLabel *label_6;
    QLineEdit *srvLine;
    QLabel *creditslab;
    QLabel *label_16;
    QLineEdit *portLine;
    QGroupBox *groupBox_4;
    QRadioButton *pyGoRadio;
    QRadioButton *fedOneRadio;
    QWidget *page_2;
    QGroupBox *groupBox;
    QLineEdit *waveTitleLine;
    QLabel *label_4;
    QPushButton *createWaveButton;
    QGroupBox *wavelistBox;
    QListView *listView;
    QWidget *page_3;
    QGroupBox *SelectedWave;
    QTextEdit *blipBox;
    QPushButton *postNewBlip;
    QPushButton *editBlip;
    QPushButton *showOnMap;
    QFrame *blipframe;
    QLabel *label_3;
    QLabel *label_14;
    QPushButton *pushButton;
    QPushButton *InviteNewUser;
    QLineEdit *inviteNewUser;
    QPushButton *deleteSelectedBlip;
    QWidget *page_4;
    QWidget *mapBox;
    QPushButton *setLocation;
    QSlider *mapZoomSlider;
    QLabel *label_13;
    QPushButton *addBlipFromMap;
    QComboBox *enterLocations;
    QPushButton *gotolocation;
    QLabel *label_15;
    QWidget *page_5;
    QGroupBox *groupBox_2;
    QLabel *label_5;
    QLabel *label_7;
    QLabel *label_8;
    QLineEdit *xPosEdit;
    QLineEdit *yPosEdit;
    QLineEdit *zPosEdit;
    QGroupBox *groupBox_3;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_11;
    QLineEdit *yawEdit;
    QLineEdit *pitchEdit;
    QLineEdit *rollEdit;
    QCheckBox *facingSpriteCheckBox;
    QLabel *label_12;
    QLineEdit *arblipDataEdit;
    QPushButton *submitNewBlip;
    QPushButton *cancelSubmit;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuSelect_Server;
    QMenu *menuWave_Shortcuts;
    QMenu *menuLocation;
    QMenu *menuSet_range_limit;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(600, 400);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(600, 400));
        MainWindow->setMaximumSize(QSize(600, 400));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/windowicon.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        action_Quit = new QAction(MainWindow);
        action_Quit->setObjectName(QString::fromUtf8("action_Quit"));
        actionLogout = new QAction(MainWindow);
        actionLogout->setObjectName(QString::fromUtf8("actionLogout"));
        action_show_list = new QAction(MainWindow);
        action_show_list->setObjectName(QString::fromUtf8("action_show_list"));
        action_show_list->setEnabled(false);
        actionOpen_wave = new QAction(MainWindow);
        actionOpen_wave->setObjectName(QString::fromUtf8("actionOpen_wave"));
        actionOpen_wave->setChecked(false);
        actionPygowaveConnect = new QAction(MainWindow);
        actionPygowaveConnect->setObjectName(QString::fromUtf8("actionPygowaveConnect"));
        localSeverConnect = new QAction(MainWindow);
        localSeverConnect->setObjectName(QString::fromUtf8("localSeverConnect"));
        action_show_active_wave = new QAction(MainWindow);
        action_show_active_wave->setObjectName(QString::fromUtf8("action_show_active_wave"));
        testSeverConnect = new QAction(MainWindow);
        testSeverConnect->setObjectName(QString::fromUtf8("testSeverConnect"));
        setClientLocationZero = new QAction(MainWindow);
        setClientLocationZero->setObjectName(QString::fromUtf8("setClientLocationZero"));
        setClientLocationTilburg = new QAction(MainWindow);
        setClientLocationTilburg->setObjectName(QString::fromUtf8("setClientLocationTilburg"));
        action_current_real_location = new QAction(MainWindow);
        action_current_real_location->setObjectName(QString::fromUtf8("action_current_real_location"));
        action_current_real_location->setEnabled(false);
        setRangeLimit50m = new QAction(MainWindow);
        setRangeLimit50m->setObjectName(QString::fromUtf8("setRangeLimit50m"));
        setRangeLimit100m = new QAction(MainWindow);
        setRangeLimit100m->setObjectName(QString::fromUtf8("setRangeLimit100m"));
        setRangeLimit200m = new QAction(MainWindow);
        setRangeLimit200m->setObjectName(QString::fromUtf8("setRangeLimit200m"));
        setRangeLimit500m = new QAction(MainWindow);
        setRangeLimit500m->setObjectName(QString::fromUtf8("setRangeLimit500m"));
        setRangeLimit1km = new QAction(MainWindow);
        setRangeLimit1km->setObjectName(QString::fromUtf8("setRangeLimit1km"));
        setRangeLimit5km = new QAction(MainWindow);
        setRangeLimit5km->setObjectName(QString::fromUtf8("setRangeLimit5km"));
        setRangeLimit50km = new QAction(MainWindow);
        setRangeLimit50km->setObjectName(QString::fromUtf8("setRangeLimit50km"));
        setRangeLimit100km = new QAction(MainWindow);
        setRangeLimit100km->setObjectName(QString::fromUtf8("setRangeLimit100km"));
        setRangeLimitInfinite = new QAction(MainWindow);
        setRangeLimitInfinite->setObjectName(QString::fromUtf8("setRangeLimitInfinite"));
        setClientLocationNewYork = new QAction(MainWindow);
        setClientLocationNewYork->setObjectName(QString::fromUtf8("setClientLocationNewYork"));
        setClientLocationLondon = new QAction(MainWindow);
        setClientLocationLondon->setObjectName(QString::fromUtf8("setClientLocationLondon"));
        actionOrient_lodge = new QAction(MainWindow);
        actionOrient_lodge->setObjectName(QString::fromUtf8("actionOrient_lodge"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setGeometry(QRect(0, 0, 601, 361));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        usrLine = new QLineEdit(page);
        usrLine->setObjectName(QString::fromUtf8("usrLine"));
        usrLine->setGeometry(QRect(90, 70, 291, 23));
        loginButton = new QPushButton(page);
        loginButton->setObjectName(QString::fromUtf8("loginButton"));
        loginButton->setGeometry(QRect(390, 40, 83, 27));
        label = new QLabel(page);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 70, 71, 17));
        passLine = new QLineEdit(page);
        passLine->setObjectName(QString::fromUtf8("passLine"));
        passLine->setGeometry(QRect(90, 100, 291, 23));
        passLine->setEchoMode(QLineEdit::Password);
        label_2 = new QLabel(page);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(20, 100, 71, 17));
        label_6 = new QLabel(page);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(40, 40, 51, 17));
        srvLine = new QLineEdit(page);
        srvLine->setObjectName(QString::fromUtf8("srvLine"));
        srvLine->setGeometry(QRect(90, 40, 291, 23));
        creditslab = new QLabel(page);
        creditslab->setObjectName(QString::fromUtf8("creditslab"));
        creditslab->setGeometry(QRect(10, 240, 381, 111));
        label_16 = new QLabel(page);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setGeometry(QRect(50, 130, 31, 17));
        portLine = new QLineEdit(page);
        portLine->setObjectName(QString::fromUtf8("portLine"));
        portLine->setGeometry(QRect(90, 130, 291, 23));
        portLine->setEchoMode(QLineEdit::Normal);
        groupBox_4 = new QGroupBox(page);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setGeometry(QRect(50, 160, 331, 51));
        pyGoRadio = new QRadioButton(groupBox_4);
        pyGoRadio->setObjectName(QString::fromUtf8("pyGoRadio"));
        pyGoRadio->setGeometry(QRect(10, 20, 101, 22));
        fedOneRadio = new QRadioButton(groupBox_4);
        fedOneRadio->setObjectName(QString::fromUtf8("fedOneRadio"));
        fedOneRadio->setGeometry(QRect(110, 20, 101, 22));
        fedOneRadio->setChecked(true);
        stackedWidget->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        groupBox = new QGroupBox(page_2);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 0, 571, 51));
        waveTitleLine = new QLineEdit(groupBox);
        waveTitleLine->setObjectName(QString::fromUtf8("waveTitleLine"));
        waveTitleLine->setGeometry(QRect(70, 15, 381, 23));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 15, 71, 21));
        createWaveButton = new QPushButton(groupBox);
        createWaveButton->setObjectName(QString::fromUtf8("createWaveButton"));
        createWaveButton->setGeometry(QRect(460, 15, 91, 21));
        wavelistBox = new QGroupBox(page_2);
        wavelistBox->setObjectName(QString::fromUtf8("wavelistBox"));
        wavelistBox->setGeometry(QRect(10, 50, 571, 301));
        listView = new QListView(wavelistBox);
        listView->setObjectName(QString::fromUtf8("listView"));
        listView->setGeometry(QRect(10, 20, 551, 271));
        stackedWidget->addWidget(page_2);
        page_3 = new QWidget();
        page_3->setObjectName(QString::fromUtf8("page_3"));
        SelectedWave = new QGroupBox(page_3);
        SelectedWave->setObjectName(QString::fromUtf8("SelectedWave"));
        SelectedWave->setGeometry(QRect(10, 0, 581, 351));
        blipBox = new QTextEdit(SelectedWave);
        blipBox->setObjectName(QString::fromUtf8("blipBox"));
        blipBox->setGeometry(QRect(10, 270, 471, 71));
        postNewBlip = new QPushButton(SelectedWave);
        postNewBlip->setObjectName(QString::fromUtf8("postNewBlip"));
        postNewBlip->setEnabled(true);
        postNewBlip->setGeometry(QRect(480, 30, 91, 21));
        editBlip = new QPushButton(SelectedWave);
        editBlip->setObjectName(QString::fromUtf8("editBlip"));
        editBlip->setEnabled(false);
        editBlip->setGeometry(QRect(490, 320, 81, 21));
        showOnMap = new QPushButton(SelectedWave);
        showOnMap->setObjectName(QString::fromUtf8("showOnMap"));
        showOnMap->setEnabled(true);
        showOnMap->setGeometry(QRect(490, 265, 81, 21));
        blipframe = new QFrame(SelectedWave);
        blipframe->setObjectName(QString::fromUtf8("blipframe"));
        blipframe->setGeometry(QRect(0, 50, 581, 201));
        blipframe->setFrameShape(QFrame::NoFrame);
        blipframe->setFrameShadow(QFrame::Plain);
        label_3 = new QLabel(SelectedWave);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(10, 40, 221, 16));
        label_14 = new QLabel(SelectedWave);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setGeometry(QRect(10, 250, 111, 16));
        pushButton = new QPushButton(SelectedWave);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setEnabled(false);
        pushButton->setGeometry(QRect(490, 293, 81, 21));
        InviteNewUser = new QPushButton(SelectedWave);
        InviteNewUser->setObjectName(QString::fromUtf8("InviteNewUser"));
        InviteNewUser->setGeometry(QRect(240, 20, 81, 21));
        inviteNewUser = new QLineEdit(SelectedWave);
        inviteNewUser->setObjectName(QString::fromUtf8("inviteNewUser"));
        inviteNewUser->setGeometry(QRect(10, 20, 221, 21));
        deleteSelectedBlip = new QPushButton(SelectedWave);
        deleteSelectedBlip->setObjectName(QString::fromUtf8("deleteSelectedBlip"));
        deleteSelectedBlip->setEnabled(true);
        deleteSelectedBlip->setGeometry(QRect(370, 30, 101, 21));
        stackedWidget->addWidget(page_3);
        page_4 = new QWidget();
        page_4->setObjectName(QString::fromUtf8("page_4"));
        mapBox = new QWidget(page_4);
        mapBox->setObjectName(QString::fromUtf8("mapBox"));
        mapBox->setGeometry(QRect(-10, -10, 620, 321));
        mapBox->setMinimumSize(QSize(600, 0));
        setLocation = new QPushButton(page_4);
        setLocation->setObjectName(QString::fromUtf8("setLocation"));
        setLocation->setGeometry(QRect(10, 335, 91, 21));
        mapZoomSlider = new QSlider(page_4);
        mapZoomSlider->setObjectName(QString::fromUtf8("mapZoomSlider"));
        mapZoomSlider->setGeometry(QRect(250, 335, 301, 20));
        mapZoomSlider->setOrientation(Qt::Horizontal);
        label_13 = new QLabel(page_4);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setGeometry(QRect(210, 335, 41, 21));
        addBlipFromMap = new QPushButton(page_4);
        addBlipFromMap->setObjectName(QString::fromUtf8("addBlipFromMap"));
        addBlipFromMap->setGeometry(QRect(110, 335, 91, 21));
        enterLocations = new QComboBox(page_4);
        enterLocations->setObjectName(QString::fromUtf8("enterLocations"));
        enterLocations->setGeometry(QRect(110, 310, 391, 21));
        enterLocations->setEditable(true);
        enterLocations->setInsertPolicy(QComboBox::InsertAtTop);
        gotolocation = new QPushButton(page_4);
        gotolocation->setObjectName(QString::fromUtf8("gotolocation"));
        gotolocation->setGeometry(QRect(510, 310, 75, 21));
        label_15 = new QLabel(page_4);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setGeometry(QRect(10, 310, 81, 20));
        stackedWidget->addWidget(page_4);
        page_5 = new QWidget();
        page_5->setObjectName(QString::fromUtf8("page_5"));
        groupBox_2 = new QGroupBox(page_5);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(10, 10, 221, 71));
        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(40, 20, 20, 20));
        label_7 = new QLabel(groupBox_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(110, 20, 21, 17));
        label_8 = new QLabel(groupBox_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(170, 20, 21, 17));
        xPosEdit = new QLineEdit(groupBox_2);
        xPosEdit->setObjectName(QString::fromUtf8("xPosEdit"));
        xPosEdit->setGeometry(QRect(10, 40, 61, 23));
        yPosEdit = new QLineEdit(groupBox_2);
        yPosEdit->setObjectName(QString::fromUtf8("yPosEdit"));
        yPosEdit->setGeometry(QRect(80, 40, 61, 23));
        zPosEdit = new QLineEdit(groupBox_2);
        zPosEdit->setObjectName(QString::fromUtf8("zPosEdit"));
        zPosEdit->setGeometry(QRect(150, 40, 61, 23));
        groupBox_3 = new QGroupBox(page_5);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(240, 10, 331, 71));
        label_9 = new QLabel(groupBox_3);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(10, 20, 41, 17));
        label_10 = new QLabel(groupBox_3);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(70, 20, 41, 17));
        label_11 = new QLabel(groupBox_3);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setGeometry(QRect(130, 20, 41, 17));
        yawEdit = new QLineEdit(groupBox_3);
        yawEdit->setObjectName(QString::fromUtf8("yawEdit"));
        yawEdit->setGeometry(QRect(10, 40, 41, 23));
        pitchEdit = new QLineEdit(groupBox_3);
        pitchEdit->setObjectName(QString::fromUtf8("pitchEdit"));
        pitchEdit->setGeometry(QRect(70, 40, 41, 23));
        rollEdit = new QLineEdit(groupBox_3);
        rollEdit->setObjectName(QString::fromUtf8("rollEdit"));
        rollEdit->setGeometry(QRect(130, 40, 41, 23));
        facingSpriteCheckBox = new QCheckBox(groupBox_3);
        facingSpriteCheckBox->setObjectName(QString::fromUtf8("facingSpriteCheckBox"));
        facingSpriteCheckBox->setGeometry(QRect(190, 40, 101, 22));
        label_12 = new QLabel(page_5);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setGeometry(QRect(10, 100, 60, 17));
        arblipDataEdit = new QLineEdit(page_5);
        arblipDataEdit->setObjectName(QString::fromUtf8("arblipDataEdit"));
        arblipDataEdit->setGeometry(QRect(10, 120, 571, 151));
        submitNewBlip = new QPushButton(page_5);
        submitNewBlip->setObjectName(QString::fromUtf8("submitNewBlip"));
        submitNewBlip->setGeometry(QRect(170, 280, 91, 31));
        cancelSubmit = new QPushButton(page_5);
        cancelSubmit->setObjectName(QString::fromUtf8("cancelSubmit"));
        cancelSubmit->setGeometry(QRect(270, 280, 91, 31));
        stackedWidget->addWidget(page_5);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 600, 26));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuSelect_Server = new QMenu(menuFile);
        menuSelect_Server->setObjectName(QString::fromUtf8("menuSelect_Server"));
        menuWave_Shortcuts = new QMenu(menuBar);
        menuWave_Shortcuts->setObjectName(QString::fromUtf8("menuWave_Shortcuts"));
        menuWave_Shortcuts->setEnabled(true);
        menuLocation = new QMenu(menuBar);
        menuLocation->setObjectName(QString::fromUtf8("menuLocation"));
        menuSet_range_limit = new QMenu(menuLocation);
        menuSet_range_limit->setObjectName(QString::fromUtf8("menuSet_range_limit"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        statusBar->setStyleSheet(QString::fromUtf8(""));
        MainWindow->setStatusBar(statusBar);
        QWidget::setTabOrder(usrLine, passLine);
        QWidget::setTabOrder(passLine, loginButton);
        QWidget::setTabOrder(loginButton, waveTitleLine);
        QWidget::setTabOrder(waveTitleLine, createWaveButton);
        QWidget::setTabOrder(createWaveButton, blipBox);
        QWidget::setTabOrder(blipBox, postNewBlip);
        QWidget::setTabOrder(postNewBlip, editBlip);
        QWidget::setTabOrder(editBlip, showOnMap);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuWave_Shortcuts->menuAction());
        menuBar->addAction(menuLocation->menuAction());
        menuFile->addAction(action_Quit);
        menuFile->addAction(actionLogout);
        menuFile->addAction(menuSelect_Server->menuAction());
        menuSelect_Server->addAction(actionPygowaveConnect);
        menuSelect_Server->addAction(localSeverConnect);
        menuSelect_Server->addAction(testSeverConnect);
        menuSelect_Server->addAction(actionOrient_lodge);
        menuWave_Shortcuts->addSeparator();
        menuWave_Shortcuts->addAction(action_show_list);
        menuWave_Shortcuts->addAction(action_show_active_wave);
        menuLocation->addAction(setClientLocationZero);
        menuLocation->addAction(setClientLocationTilburg);
        menuLocation->addAction(setClientLocationLondon);
        menuLocation->addAction(setClientLocationNewYork);
        menuLocation->addSeparator();
        menuLocation->addAction(menuSet_range_limit->menuAction());
        menuLocation->addSeparator();
        menuLocation->addAction(action_current_real_location);
        menuSet_range_limit->addAction(setRangeLimit50m);
        menuSet_range_limit->addAction(setRangeLimit100m);
        menuSet_range_limit->addAction(setRangeLimit200m);
        menuSet_range_limit->addAction(setRangeLimit500m);
        menuSet_range_limit->addAction(setRangeLimit1km);
        menuSet_range_limit->addAction(setRangeLimit5km);
        menuSet_range_limit->addAction(setRangeLimit50km);
        menuSet_range_limit->addAction(setRangeLimit100km);
        menuSet_range_limit->addSeparator();
        menuSet_range_limit->addAction(setRangeLimitInfinite);

        retranslateUi(MainWindow);
        QObject::connect(action_Quit, SIGNAL(activated()), MainWindow, SLOT(close()));

        stackedWidget->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        action_Quit->setText(QApplication::translate("MainWindow", "&Quit", 0, QApplication::UnicodeUTF8));
        actionLogout->setText(QApplication::translate("MainWindow", "Log Out", 0, QApplication::UnicodeUTF8));
        action_show_list->setText(QApplication::translate("MainWindow", "(show my waves)", 0, QApplication::UnicodeUTF8));
        actionOpen_wave->setText(QApplication::translate("MainWindow", "open wave", 0, QApplication::UnicodeUTF8));
        actionPygowaveConnect->setText(QApplication::translate("MainWindow", "Pygowave.net", 0, QApplication::UnicodeUTF8));
        localSeverConnect->setText(QApplication::translate("MainWindow", "192.168.56.3", 0, QApplication::UnicodeUTF8));
        action_show_active_wave->setText(QApplication::translate("MainWindow", "(show active wave)", 0, QApplication::UnicodeUTF8));
        testSeverConnect->setText(QApplication::translate("MainWindow", "(test mode)", 0, QApplication::UnicodeUTF8));
        setClientLocationZero->setText(QApplication::translate("MainWindow", "0/0/0", 0, QApplication::UnicodeUTF8));
        setClientLocationTilburg->setText(QApplication::translate("MainWindow", "Tilburg", 0, QApplication::UnicodeUTF8));
        action_current_real_location->setText(QApplication::translate("MainWindow", "(current real location)", 0, QApplication::UnicodeUTF8));
        setRangeLimit50m->setText(QApplication::translate("MainWindow", "50m", 0, QApplication::UnicodeUTF8));
        setRangeLimit100m->setText(QApplication::translate("MainWindow", "100m", 0, QApplication::UnicodeUTF8));
        setRangeLimit200m->setText(QApplication::translate("MainWindow", "200m", 0, QApplication::UnicodeUTF8));
        setRangeLimit500m->setText(QApplication::translate("MainWindow", "500m", 0, QApplication::UnicodeUTF8));
        setRangeLimit1km->setText(QApplication::translate("MainWindow", "1km", 0, QApplication::UnicodeUTF8));
        setRangeLimit5km->setText(QApplication::translate("MainWindow", "5km", 0, QApplication::UnicodeUTF8));
        setRangeLimit50km->setText(QApplication::translate("MainWindow", "50km", 0, QApplication::UnicodeUTF8));
        setRangeLimit100km->setText(QApplication::translate("MainWindow", "100km", 0, QApplication::UnicodeUTF8));
        setRangeLimitInfinite->setText(QApplication::translate("MainWindow", "infinite", 0, QApplication::UnicodeUTF8));
        setClientLocationNewYork->setText(QApplication::translate("MainWindow", "New York", 0, QApplication::UnicodeUTF8));
        setClientLocationLondon->setText(QApplication::translate("MainWindow", "London", 0, QApplication::UnicodeUTF8));
        actionOrient_lodge->setText(QApplication::translate("MainWindow", "orient-lodge", 0, QApplication::UnicodeUTF8));
        loginButton->setText(QApplication::translate("MainWindow", "Login", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "Username:", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "Password:", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("MainWindow", "Server:", 0, QApplication::UnicodeUTF8));
        srvLine->setText(QApplication::translate("MainWindow", "pygowave.net", 0, QApplication::UnicodeUTF8));
        creditslab->setText(QApplication::translate("MainWindow", "AR Wave demo client.\n"
"Coding by;\n"
"Davide Carnovale,\n"
"Thomas Wrobel\n"
"\n"
"Using;\n"
"Pygowave API, QMapControll", 0, QApplication::UnicodeUTF8));
        label_16->setText(QApplication::translate("MainWindow", "Port:", 0, QApplication::UnicodeUTF8));
        portLine->setInputMask(QString());
        portLine->setText(QApplication::translate("MainWindow", "<leave blank for default>", 0, QApplication::UnicodeUTF8));
        groupBox_4->setTitle(QApplication::translate("MainWindow", "Server type", 0, QApplication::UnicodeUTF8));
        pyGoRadio->setText(QApplication::translate("MainWindow", "PyGoWave", 0, QApplication::UnicodeUTF8));
        fedOneRadio->setText(QApplication::translate("MainWindow", "FedOne", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("MainWindow", " Create a new wave ", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MainWindow", "Wave title:", 0, QApplication::UnicodeUTF8));
        createWaveButton->setText(QApplication::translate("MainWindow", "Create", 0, QApplication::UnicodeUTF8));
        wavelistBox->setTitle(QApplication::translate("MainWindow", "Your Subscribed Waves:", 0, QApplication::UnicodeUTF8));
        SelectedWave->setTitle(QApplication::translate("MainWindow", "Selected Wave", 0, QApplication::UnicodeUTF8));
        postNewBlip->setText(QApplication::translate("MainWindow", "Post New Blip", 0, QApplication::UnicodeUTF8));
        editBlip->setText(QApplication::translate("MainWindow", "Post Changes", 0, QApplication::UnicodeUTF8));
        showOnMap->setText(QApplication::translate("MainWindow", "Show On Map", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MainWindow", "Blips In Wave:", 0, QApplication::UnicodeUTF8));
        label_14->setText(QApplication::translate("MainWindow", "Selected Blip:", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("MainWindow", "Edit Blip", 0, QApplication::UnicodeUTF8));
        InviteNewUser->setText(QApplication::translate("MainWindow", "Invite new user", 0, QApplication::UnicodeUTF8));
        deleteSelectedBlip->setText(QApplication::translate("MainWindow", "Delete Selected Blip", 0, QApplication::UnicodeUTF8));
        setLocation->setText(QApplication::translate("MainWindow", "Move Blip Here", 0, QApplication::UnicodeUTF8));
        label_13->setText(QApplication::translate("MainWindow", "Zoom:", 0, QApplication::UnicodeUTF8));
        addBlipFromMap->setText(QApplication::translate("MainWindow", "Add Blip Here", 0, QApplication::UnicodeUTF8));
        gotolocation->setText(QApplication::translate("MainWindow", "Goto Location", 0, QApplication::UnicodeUTF8));
        label_15->setText(QApplication::translate("MainWindow", "Enter a map URL:", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Location", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("MainWindow", "X:", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("MainWindow", "Y:", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("MainWindow", "Z:", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "Orientation", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("MainWindow", "Yaw:", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("MainWindow", "Pitch:", 0, QApplication::UnicodeUTF8));
        label_11->setText(QApplication::translate("MainWindow", "Roll:", 0, QApplication::UnicodeUTF8));
        facingSpriteCheckBox->setText(QApplication::translate("MainWindow", "Facing Sprite", 0, QApplication::UnicodeUTF8));
        label_12->setText(QApplication::translate("MainWindow", "Data:", 0, QApplication::UnicodeUTF8));
        submitNewBlip->setText(QApplication::translate("MainWindow", "Submit", 0, QApplication::UnicodeUTF8));
        cancelSubmit->setText(QApplication::translate("MainWindow", "Cancel", 0, QApplication::UnicodeUTF8));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0, QApplication::UnicodeUTF8));
        menuSelect_Server->setTitle(QApplication::translate("MainWindow", "Select Server", 0, QApplication::UnicodeUTF8));
        menuWave_Shortcuts->setTitle(QApplication::translate("MainWindow", "Wave Shortcuts", 0, QApplication::UnicodeUTF8));
        menuLocation->setTitle(QApplication::translate("MainWindow", "Client Location", 0, QApplication::UnicodeUTF8));
        menuSet_range_limit->setTitle(QApplication::translate("MainWindow", "set range limit", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
