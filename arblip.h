#ifndef ARBLIP_H
#define ARBLIP_H

#include <QWidget>
#include <QMap>
#include <QDateTime>
#include "location.h"

class ARBlip
{
public:
    ARBlip();
    ~ARBlip();

     ARBlip(QByteArray,QString,double,double,double,int,int,int,QString,QDateTime);

     QString getDataAsString();
     QString getEditors();
     QByteArray getRefID();
     QDateTime getLastUpdateTime();
     //This convert to and from string. used for storage in blip/annotation
     QString storeToString();
     void loadFromString(QString);

     location getLoc();

     double getX();
     double getY();
     double getZ();

     QString getXAsString();
     QString getYAsString();
     QString getZAsString();

     void setX(double);
     void setY(double);
     void setZ(double);

     void setReferanceID(QByteArray);
     bool isFaceingSprite();

    QString getParentWave();
    void setParentWave(QByteArray);

private:

     //ID referance. This would be a unique indentifier for the blip. Presumably the same as Wave uses itself.
     QByteArray ReferanceID;

    //Last editor(s)
     QString Editors;


    location Loc;

    int Rotation;
    int Pitch;
    int Yaw;
    bool FacingSprite; //if no rotation specified, this should default to true
                       //if set to true when a rotation is set, then it keeps that rotation relative to the viewer
                       //not relative to the earth.

    //Data format
    QString DataMIME;
    QString CoordinateSystemUsed;

    //Data itself
    QString Data;
    QDateTime *BlipLastUpdateTimestamp; //this would be the last time any blip was updated on the wave
    QString DataUpdatedTimestamp;    //Time the Data was updated/changed
                                     //Note; BlipLastUpdateTimestamp should be used for updates that dont effect the data itself.
                                     //(such as movements)
    //Data metadata
    QMap<QString, QString> Metadata;

    //Parent wave (not essential, but when dealing with multiple waves at once, this could be usefull)
    QByteArray ParentWave;

};

#endif // ARBLIP_H
