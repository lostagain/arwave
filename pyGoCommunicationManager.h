#ifndef PYGOCOMMUNICATIONMANAGER_H
#define PYGOCOMMUNICATIONMANAGER_H

#include "waveListModel.h"
#include <PyGoWaveApi/controller.h>
#include "abstractCommunicationManager.h"
#include "arbliparray.h"

//hinerits from abc(abstract base class) AbstractCommunicationManager
class PyGoCommunicationManager: public AbstractCommunicationManager
{
    Q_OBJECT

public:
    //TODO: reimplement virtual methods from AbstractCommunicationManager
    PyGoCommunicationManager(WaveListModel *wlm, ARBlipArray *arba);
    //~PyGoCommunicationManager();

    void login(QString serverAddress, QString username, QString password, int port = 61613 );
    void logout();
    void createWave(QString title);
    void openWavelet(QString waveletId);
    void closeWavelet();
    void addBlip(QString text);
    void updateBlip(QByteArray blipID, QString text);
    void deleteBlip(QByteArray blipID);
    bool addParticipant(QString participant);

public slots:
    void controller_waveAdded(const QByteArray &waveId);
    void controller_waveAboutToBeRemoved(const QByteArray &waveId);
    void wavelet_blipInserted(int index, QByteArray blipID);
    void blip_IDChanged(QByteArray oldID, QByteArray newID);
    void blip_deletedText(int index, int lenght);
    void blip_insertedText(int index, const QString &text);
    void wavelet_blipDeleted(QByteArray blipID);
    void controller_waveletOpened(const QByteArray &waveletID, bool isRoot);

signals:
    void arblipsUpdated();

private:
    //TODO: vars
    PyGoWave::Controller* m_controller;
    PyGoWave::Wavelet *m_currentWavelet;
    WaveListModel *m_waveListModel;
    ARBlipArray *m_ARBlips;

    void updateARBlipArray(const PyGoWave::Blip *blip);
};

#endif // PYGOCOMMUNICATIONMANAGER_H
