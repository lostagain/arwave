#ifndef FEDONECOMMUNICATIONMANAGER_H
#define FEDONECOMMUNICATIONMANAGER_H

#include "abstractCommunicationManager.h"
#include "QWave/waveclient/app/environment.h"
#include "QWave/waveclient/app/settings.h"
#include "QWave/waveclient/model/wavelist.h"
#include "QWave/waveclient/model/wave.h"
#include "QWave/core/model/documentmutation.h"
#include "QWave/waveclient/model/wavelet.h"
#include "QWave/waveclient/model/otprocessor.h"
#include "QWave/waveclient/model/wavedigest.h"
#include "QWave/waveclient/network/networkadapter.h"
#include "QWave/waveclient/model/blip.h"
#include "QWave/waveclient/model/blipdocument.h"
#include "waveListModel.h"
#include "arbliparray.h"

class FedOneCommunicationManager: public AbstractCommunicationManager
{
    Q_OBJECT

public:
    FedOneCommunicationManager(WaveListModel *wlm, ARBlipArray *arba);
    ~FedOneCommunicationManager();

    void login(QString serverAddress, QString username, QString password, int port = 5275 );
    void logout();
    void createWave(QString title);
    void openWavelet(QString waveletId);
    void closeWavelet();
    void addBlip(QString text);
    void updateBlip(QByteArray blipID, QString text);
    void deleteBlip(QByteArray blipID);
    bool addParticipant(QString participant);

public slots:
    void enviroment_waveAdded(Wave *wave);
    void wavelet_conversationChanged();
    void blip_updated(DocumentMutation);

signals:
    void arblipsUpdated();

private:
    Environment *m_environment;
    WaveListModel *m_waveListModel;
    ARBlipArray *m_ARBlips;
    Wavelet *m_currentWavelet;
    QMap<QString, Wave *> m_wavesMap;
};

#endif // FEDONECOMMUNICATIONMANAGER_H
