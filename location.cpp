#include "location.h"
#include "math.h"
#include <QtGlobal>
#include <QDebug>

location::location()
{
    this->Xpos = 0;
    this->Ypos = 0;
    this->Zpos = 0;

}

//some usefull functions

inline double deg2rad(double d) { return d*3.14159265358979323846/180; }

/** This is the distance to this location from another, assuming the locations are Cartesian coordinate system **/
double location::distanceToCartesian(location Loc)
{
    //Whoopie-de-do, I get to use some pythagoras

    //work out 2D displacement
    double dx =  Xpos - Loc.Xpos;
    double dy =  Ypos - Loc.Ypos;

    //work out h
    double h = pow((pow(dx,2) + pow(dy,2)),0.5);

   //now find the vertical displacement
    double dz =  Zpos - Loc.Zpos;

    //and get the distance using dz and h
    double distance = pow((pow(dz,2) + pow(h,2)),0.5);


    return distance;
}

/** This returns the distance in meters, assuming both the stored co-ordinates and the ones being compared to are Log/Lat co-ordinates specified in degrees **/
/** x/y is assumed to be lat/lon **/
double location::distanceTo(location Loc)
{

 qDebug()<<" working out distance from "<<Loc.Xpos<<","<<Loc.Ypos<<" to "<<Xpos<<","<<Ypos;

 //distance in meters
 double meters = distVincenty(Loc.Xpos,Loc.Ypos,Xpos,Ypos); 
 //double km = meters/1000;

 qDebug()<<" distance is "<<meters<<" meters";

 return meters;

}



/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
/*  Vincenty Inverse Solution of Geodesics on the Ellipsoid (c) Chris Veness 2002-2009            */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

/*
 * Calculate geodesic distance (in m) between two points specified by latitude/longitude
 * (in numeric degrees) using Vincenty inverse formula for ellipsoids
 */

// Converted From Javascript found at; http://www.movable-type.co.uk/scripts/latlong-vincenty.html
// This piece of maths (apperently) returns the location to the distance to the nearest mm

double location::distVincenty(double lat1,double lon1,double lat2,double lon2) {
  double a = 6378137, b = 6356752.3142,  f = 1/298.257223563;  // WGS-84 ellipsiod
  double L = deg2rad(lon2-lon1);

  qDebug()<<"L="<<L;

  double U1 = atan((1-f) * tan(deg2rad(lat1)));
  double U2 = atan((1-f) * tan(deg2rad(lat2)));
  double sinU1 = sin(U1), cosU1 = cos(U1);
  double sinU2 = sin(U2), cosU2 = cos(U2);

  double lambda = L, lambdaP, iterLimit = 100;



  //declare
  double sigma;
  double cosSigma;
  double cosSqAlpha;
  double sinSigma;
  double cos2SigmaM;
  double sinAlpha;
  double sinLambda;
  double cosLambda;
double C;
  do {

      qDebug()<<"looping";

     sinLambda = sin(lambda);
     cosLambda = cos(lambda);
     sinSigma = sqrt((cosU2*sinLambda) * (cosU2*sinLambda) +
      (cosU1*sinU2-sinU1*cosU2*cosLambda) * (cosU1*sinU2-sinU1*cosU2*cosLambda));
    if (sinSigma==0) return 0;  // co-incident points
     cosSigma = sinU1*sinU2 + cosU1*cosU2*cosLambda;
     sigma = atan2(sinSigma, cosSigma);
     sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
     cosSqAlpha = 1 - sinAlpha*sinAlpha;
     cos2SigmaM = cosSigma - 2*sinU1*sinU2/cosSqAlpha;
    if (isnan(cos2SigmaM)) cos2SigmaM = 0;  // equatorial line: cosSqAlpha=0 (�6)
     C = f/16*cosSqAlpha*(4+f*(4-3*cosSqAlpha));
    lambdaP = lambda;
    lambda = L + (1-C) * f * sinAlpha *
      (sigma + C*sinSigma*(cos2SigmaM+C*cosSigma*(-1+2*cos2SigmaM*cos2SigmaM)));
  } while (qAbs(lambda-lambdaP) > 1e-12 && --iterLimit>0);

  if (iterLimit==0) return false;  // this was NaN...not sure if false works as a replacement :-/


  qDebug()<<" cosSqAlpha="<<cosSqAlpha;

  double uSq = cosSqAlpha * (a*a - b*b) / (b*b);

  qDebug()<<" uSq="<<uSq;
  double A = 1 + uSq/16384*(4096+uSq*(-768+uSq*(320-175*uSq)));

  qDebug()<<" A="<<A;

  double B = uSq/1024 * (256+uSq*(-128+uSq*(74-47*uSq)));  

   qDebug()<<" B="<<B;

  double deltaSigma = B*sinSigma*(cos2SigmaM+B/4*(cosSigma*(-1+2*cos2SigmaM*cos2SigmaM)-
    B/6*cos2SigmaM*(-3+4*sinSigma*sinSigma)*(-3+4*cos2SigmaM*cos2SigmaM)));


  qDebug()<<" deltaSigma="<<deltaSigma;

  double s = b*A*(sigma-deltaSigma);


  qDebug()<<" s="<<s;

  //s = s.toFixed(3); // round to 1mm precision
  s = ((round(s*1000))/1000);

  qDebug()<<" s="<<s;


  return s;

}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

/*
 * extend String object with method for parsing degrees or lat/long values to numeric degrees
 *
 * this is very flexible on formats, allowing signed decimal degrees, or deg-min-sec suffixed by
 * compass direction (NSEW). A variety of separators are accepted (eg 3� 37' 09"W) or fixed-width
 * format without separators (eg 0033709W). Seconds and minutes may be omitted. (Minimal validation
 * is done).
 */
/*
String.prototype.parseDeg = function() {
  if (!isNaN(this)) return Number(this);                 // signed decimal degrees without NSEW

  double degLL = this.replace(/^-/,'').replace(/[NSEW]/i,'');  // strip off any sign or compass dir'n
  double dms = degLL.split(/[^0-9.]+/);                     // split out separate d/m/s
  for (var i in dms) if (dms[i]=='') dms.splice(i,1);    // remove empty elements (see note below)
  switch (dms.length) {                                  // convert to decimal degrees...
    case 3:                                              // interpret 3-part result as d/m/s
      var deg = dms[0]/1 + dms[1]/60 + dms[2]/3600; break;
    case 2:                                              // interpret 2-part result as d/m
      var deg = dms[0]/1 + dms[1]/60; break;
    case 1:                                              // decimal or non-separated dddmmss
      if (/[NS]/i.test(this)) degLL = '0' + degLL;       // - normalise N/S to 3-digit degrees
      var deg = dms[0].slice(0,3)/1 + dms[0].slice(3,5)/60 + dms[0].slice(5)/3600; break;
    default: return NaN;
  }
  if (/^-/.test(this) || /[WS]/i.test(this)) deg = -deg; // take '-', west and south as -ve
  return deg;
}
// note: whitespace at start/end will split() into empty elements (except in IE)

/*
 * extend Number object with methods for converting degrees/radians
*/
/*
Number.prototype.toRad = function() {  // convert degrees to radians
  return this * Math.PI / 180;
}
*/

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
