#ifndef WAVEMODEL_H
#define WAVEMODEL_H

#include <QString>
#include <QDateTime>

//this class defines a single wave object
//FIXME: should we need to care about wavelets and blips here?
class WaveModel: QObject
{
    //required for signal/slot mechanism
    Q_OBJECT

public:
    WaveModel();
    WaveModel(QString id, QDateTime lastUpdate, QString title);
    ~WaveModel();
    //TODO: needed methods, not implemented yet
    //return the id of the wave
    QString id();
    //sets the id of the wave
    void setId(QString id);
    //gets the last update timeDate
    QDateTime lastChange();
    //get the title of the wave
    QString title();
    //set the title of the wave
    void setTitle(QString title);

    //TODO: needed signals/slots
    //?
//public slots:
    //void lastChangeUpdated( QDateTime lastUpdate );

private:
    //TODO: needed vars
    //the pygo api stores the id of the wave, the id of the client, a list of wavelets, the root wavelet and a participant something
    //the c++ fedone stores the id of the wave, the domain, the last change time and a waveDigest something
    QString m_id;
    QDateTime m_lastChange;
    //FIXME: find a better name for this var?!
    QString m_title;
};

#endif // WAVEMODEL_H
