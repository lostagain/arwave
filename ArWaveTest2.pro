# -------------------------------------------------
# Project created by QtCreator 2009-12-21T17:07:03
# -------------------------------------------------
include(./QMapControl/QMapControl.pri)
DEPENDPATH += ./QMapControl/src
INCLUDEPATH += ./QMapControl/src
TARGET = ARWaveTest1
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    waveconnectionicon.cpp \
    arblip.cpp \
    arbliplistitem.cpp \
    arbliparray.cpp \
    location.cpp \
    arbliplist.cpp \
    pyGoCommunicationManager.cpp \
    waveModel.cpp \
    waveListModel.cpp \
    fedonecommunicationmanager.cpp \
    QWave/waveclient/model/wavelet.cpp \
    QWave/waveclient/model/wave.cpp \
    QWave/waveclient/model/participant.cpp \
    QWave/waveclient/model/blip.cpp \
    QWave/waveclient/model/blipthread.cpp \
    QWave/core/model/structureddocument.cpp \
    QWave/waveclient/view/waveletview.cpp \
    QWave/waveclient/view/blipview.cpp \
    QWave/waveclient/view/waveview.cpp \
    QWave/core/model/documentmutation.cpp \
    QWave/waveclient/view/blipgraphicsitem.cpp \
    QWave/waveclient/view/waveletgraphicsitem.cpp \
    QWave/waveclient/view/participantgraphicsitem.cpp \
    QWave/waveclient/view/caret.cpp \
    QWave/waveclient/app/environment.cpp \
    QWave/waveclient/view/graphicstextitem.cpp \
    QWave/waveclient/view/otadapter.cpp \
    QWave/waveclient/network/networkadapter.cpp \
    QWave/core/network/rpc.cpp \
    QWave/waveclient/view/wavelistview.cpp \
    QWave/waveclient/model/wavelist.cpp \
    QWave/waveclient/view/wavedigestgraphicsitem.cpp \
    QWave/waveclient/app/serversettingsdialog.cpp \
    QWave/core/model/waveletdelta.cpp \
    QWave/waveclient/model/otprocessor.cpp \
    QWave/core/model/waveletdeltaoperation.cpp \
    QWave/waveclient/model/wavedigest.cpp \
    QWave/waveclient/model/contacts.cpp \
    QWave/waveclient/view/contactsview.cpp \
    QWave/waveclient/view/searchbox.cpp \
    QWave/waveclient/view/participantlistview.cpp \
    QWave/waveclient/view/titlebar.cpp \
    QWave/waveclient/view/inboxview.cpp \
    QWave/waveclient/view/bigbar.cpp \
    QWave/waveclient/view/addparticipantdialog.cpp \
    QWave/waveclient/view/buttongraphicsitem.cpp \
    QWave/waveclient/app/settings.cpp \
    QWave/waveclient/model/unknowndocument.cpp \
    QWave/waveclient/model/blipdocument.cpp \
    QWave/waveclient/view/popupdialog.cpp \
    QWave/waveclient/view/participantinfodialog.cpp \
    QWave/waveclient/view/insertimagedialog.cpp \
    QWave/waveclient/view/imagehandler.cpp \
    QWave/waveclient/model/attachment.cpp \
    QWave/waveclient/view/toolbar.cpp \
    QWave/core/network/converter.cpp \
    QWave/waveclient/gadgets/gadgetmanifest.cpp \
    QWave/waveclient/gadgets/gadgetapi.cpp \
    QWave/waveclient/gadgets/gadgethandler.cpp \
    QWave/waveclient/gadgets/gadgetview.cpp \
    QWave/waveclient/gadgets/extensionmanifest.cpp \
    QWave/waveclient/view/inboxbuttonview.cpp \
    QWave/core/model/waveurl.cpp \
    QWave/waveclient/view/insertgadgetdialog.cpp \
    QWave/core/protocol/waveclient-rpc.pb.cc \
    QWave/core/protocol/common.pb.cc
HEADERS += mainwindow.h \
    waveconnectionicon.h \
    wavelistitem.h \
    arblip.h \
    arbliplistitem.h \
    arbliparray.h \
    location.h \
    arbliplist.h \
    qmapcontrol.h \
    waveModel.h \
    waveListModel.h \
    abstractCommunicationManager.h \
    fedonecommunicationmanager.h \
    pyGoCommunicationManager.h \
    QWave/waveclient/model/wavelet.h \
    QWave/waveclient/model/wave.h \
    QWave/waveclient/model/participant.h \
    QWave/waveclient/model/blip.h \
    QWave/waveclient/model/blipthread.h \
    QWave/core/model/structureddocument.h \
    QWave/waveclient/view/waveletview.h \
    QWave/waveclient/view/blipview.h \
    QWave/waveclient/view/waveview.h \
    QWave/core/model/documentmutation.h \
    QWave/waveclient/view/blipgraphicsitem.h \
    QWave/waveclient/view/waveletgraphicsitem.h \
    QWave/waveclient/view/participantgraphicsitem.h \
    QWave/waveclient/view/caret.h \
    QWave/waveclient/app/environment.h \
    QWave/waveclient/view/graphicstextitem.h \
    QWave/waveclient/view/otadapter.h \
    QWave/waveclient/network/networkadapter.h \
    QWave/core/network/rpc.h \
    QWave/waveclient/view/wavelistview.h \
    QWave/waveclient/model/wavelist.h \
    QWave/waveclient/view/wavedigestgraphicsitem.h \
    QWave/waveclient/app/serversettingsdialog.h \
    QWave/core/model/waveletdelta.h \
    QWave/waveclient/model/otprocessor.h \
    QWave/core/model/waveletdeltaoperation.h \
    QWave/waveclient/model/wavedigest.h \
    QWave/waveclient/model/contacts.h \
    QWave/waveclient/view/contactsview.h \
    QWave/waveclient/view/searchbox.h \
    QWave/waveclient/view/participantlistview.h \
    QWave/waveclient/view/titlebar.h \
    QWave/waveclient/view/inboxview.h \
    QWave/waveclient/view/bigbar.h \
    QWave/waveclient/view/addparticipantdialog.h \
    QWave/waveclient/view/buttongraphicsitem.h \
    QWave/waveclient/app/settings.h \
    QWave/waveclient/model/unknowndocument.h \
    QWave/waveclient/model/blipdocument.h \
    QWave/waveclient/view/popupdialog.h \
    QWave/waveclient/view/participantinfodialog.h \
    QWave/waveclient/view/insertimagedialog.h \
    QWave/waveclient/view/imagehandler.h \
    QWave/waveclient/model/attachment.h \
    QWave/waveclient/view/toolbar.h \
    QWave/core/network/converter.h \
    QWave/waveclient/gadgets/gadgetmanifest.h \
    QWave/waveclient/gadgets/gadgetapi.h \
    QWave/waveclient/gadgets/gadgethandler.h \
    QWave/waveclient/gadgets/gadgetview.h \
    QWave/waveclient/gadgets/extensionmanifest.h \
    QWave/waveclient/view/inboxbuttonview.h \
    QWave/core/model/waveurl.h \
    QWave/waveclient/view/insertgadgetdialog.h \
    QWave/core/protocol/waveclient-rpc.pb.h \
    QWave/core/protocol/common.pb.h
FORMS += mainwindow.ui \
    QWave/waveclient/app/serversettingsdialog.ui
INCLUDEPATH += QWave/core/ \
               QWave/core/protocol/ \
               QWave/waveclient/
win32:LIBS += -lpygowave_api0
else:LIBS += -lpygowave_api \
    -lprotobuf
OTHER_FILES +=
RESOURCES += arwaveicons.qrc
QT += webkit
QT += network
QT += xml
OTHER_FILES += javascript/gadget.js
