#include "arblip.h"
#include <QDateTime>
#include "location.h"

#include <math.h>

ARBlip::ARBlip()
{
 ReferanceID = "";
     Editors="";
    Loc.Xpos=0;
    Loc.Ypos=0;
    Loc.Zpos=0;
    Rotation = 0;
    Pitch = 0;
    Yaw = 0;
    Data = "";
    BlipLastUpdateTimestamp = new QDateTime();
    *BlipLastUpdateTimestamp = QDateTime::currentDateTime();
}

ARBlip::ARBlip(QByteArray refID,QString editors, double xpos, double ypos, double zpos, int r, int p, int y, QString data,QDateTime TimeStamp){


    ReferanceID = refID;
    Editors=editors;
    Loc.Xpos=xpos;
    Loc.Ypos=ypos;
    Loc.Zpos=zpos;
    Rotation = r;
    Pitch = p;
    Yaw = y;
    Data = data;
    BlipLastUpdateTimestamp =  new QDateTime();
    *BlipLastUpdateTimestamp = TimeStamp;

}

ARBlip::~ARBlip(){

}

//Will return either the url to the geolocated data, or just a text string if thats all the data is.
QString ARBlip::getDataAsString()
{
    return Data;
}
QString ARBlip::getEditors()
{
    return Editors;
}
QByteArray ARBlip::getRefID()
{
    return ReferanceID;
}
QDateTime ARBlip::getLastUpdateTime()
{
    return *BlipLastUpdateTimestamp;
}

location ARBlip::getLoc(){
    return Loc;
}

double ARBlip::getX()
{
    return Loc.Xpos;
}
double ARBlip::getY()
{
    return Loc.Ypos;
}
double ARBlip::getZ()
{
    return Loc.Zpos;
}

void ARBlip::setX(double x)
{
    Loc.Xpos = x;
    return;
}
void ARBlip::setY(double y)
{
     Loc.Ypos = y;
    return;
}
void ARBlip::setZ(double z)
{
    Loc.Zpos = z;
    return;
}

QString ARBlip::getXAsString()
{
    QString XasString;
    XasString.setNum(Loc.Xpos);
    return XasString;
}
QString ARBlip::getYAsString()
{
    QString YasString;
    YasString.setNum(Loc.Ypos);
    return YasString;
}
QString ARBlip::getZAsString()
{
    QString ZasString;
    ZasString.setNum(Loc.Zpos);
    return ZasString;
}
bool ARBlip::isFaceingSprite()
{
    return FacingSprite;
}


QString ARBlip::getParentWave()
{
  return ParentWave;

}
void ARBlip::setReferanceID(QByteArray newid)
{
    ReferanceID = newid;
    return;
}

void ARBlip::setParentWave(QByteArray waveID)
{
    ParentWave = waveID;

}

QString ARBlip::storeToString()
{
    QString x;
    QString y;
    QString z;
    //concatenate everything with a silly separator right now...
    return QString( x.setNum(Loc.Xpos).toLatin1() +"#"+ y.setNum(Loc.Ypos).toLatin1()
                    +"#"+ z.setNum(Loc.Zpos).toLatin1() +"#"+ Data.toLatin1() );
}

void ARBlip::loadFromString(QString data)
{
    //extract the x position
    QString x;
    x = data.mid( 0, data.indexOf("#") );
    data = data.mid( data.indexOf("#")+1, -1 ); //cut out the read data and keep going
    //extract the y position
    QString y;
    y = data.mid(0, data.indexOf("#") );
    data = data.mid( data.indexOf("#")+1, -1 ); //cut out the read data and keep going
    //extract the z position
    QString z;
    z = data.mid(0, data.indexOf("#") );
    data = data.mid( data.indexOf("#")+1, -1 ); //cut out the read data and keep going
    //all the rest is treated as content
    QString content;
    content = data;

    //set the read params to the real object
    this->Loc.Xpos = x.toDouble();
    this->Loc.Ypos = y.toDouble();
    this->Loc.Zpos = z.toDouble();
    this->Data = content;
}
