#ifndef ARBLIPLISTITEM_H
#define ARBLIPLISTITEM_H

#include <QWidget>
#include <QListWidgetItem>
#include "arblip.h"
#include "arbliplistitem.h"
#include "location.h"

QT_BEGIN_NAMESPACE
class QLabel;
QT_END_NAMESPACE

class ARBlipListItem : public QListWidgetItem
{

public:
    ARBlipListItem(ARBlip,location CurrentLoc);
    QString getBlipsDataAsText();
    QString getBlipsDataAsTextShort();
    ARBlip getARBlip();
private:
    ARBlip arblip;
};

#endif // ARBLIPLISTITEM_H
