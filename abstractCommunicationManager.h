#ifndef ABSTRACTCOMMUNICATIONMANAGER_H
#define ABSTRACTCOMMUNICATIONMANAGER_H

#include <QString>
#include <QObject>

class AbstractCommunicationManager: public QObject
{
    Q_OBJECT
public:
    //constructor with signal-slot thingy

    //login
    //the pygo api takes the server address, username and password
    //the fedone c++ takes ???
    virtual void login(QString serverAddress, QString username, QString password, int port) = 0;
    //logout
    //the pygo api takes nothing (but we need to have a working controller object)
    //the fedone c++ takes ???
    virtual void logout() = 0;

    //create wave
    //the pygo api takes the title(QString) of the wave
    //the fedone c++ takes a random ID; client generated
    virtual void createWave(QString title) = 0;

    //open wavelet
    //the pygo api takes the id (QByteArray) of the root wavelet
    //the fedone c++ takes a Wavelet* object (or waveID and wavelet ID as Strings in the sendOpenWave method)
    virtual void openWavelet(QString waveletId) = 0;
    //close wavelet
    virtual void closeWavelet() = 0;

    virtual void addBlip(QString text) = 0;

    virtual void updateBlip(QByteArray blipID, QString text) = 0;

    virtual void deleteBlip(QByteArray blipID ) = 0;

    virtual bool addParticipant(QString participant) = 0;

    //connection state (with signal?)

    //in pygo we used to have lots of signals to inform us of what's going on...
    //what to do here? can signals be virtual?
signals:
    void arblipsUpdated();

};

#endif // ABSTRACTCOMMUNICATIONMANAGER_H
