#include "waveListModel.h"

int WaveListModel::rowCount(const QModelIndex &parent) const
{
    return m_waves.size();
}

QVariant WaveListModel::data( const QModelIndex &index , int role ) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= m_waves.size())
        return QVariant();

    if (role == Qt::DisplayRole || role == Qt::EditRole)        
        return m_waves[index.row()]->id();
    else
        return QVariant();
}

//will this work?
QVariant WaveListModel::headerData ( int section, Qt::Orientation orientation, int role ) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal)
         return QString("Column %1").arg(section);
    else
         return QString("Row %1").arg(section);

}

Qt::ItemFlags WaveListModel::flags( const QModelIndex &index ) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

bool WaveListModel::setData ( const QModelIndex & index, const QVariant & value, int role )
{
    //FIXME: find a better place for this piece of code
    //qRegisterMetaType<WaveModel *>("WaveModelStar");
    if (index.isValid() && role == Qt::EditRole)
    {
        m_waves.replace(index.row(), value.value<WaveModel *>());
        emit dataChanged(index, index);
        return true;
    }
    return false;
}

bool WaveListModel::insertRows ( int position, int rows, const QModelIndex & parent )
{
    beginInsertRows(QModelIndex(), position, position+rows-1);

    for (int row = 0; row < rows; ++row)
    {
        m_waves.insert(position, new WaveModel());
    }

    endInsertRows();
    return true;
}

bool WaveListModel::removeRows ( int position, int rows, const QModelIndex & parent )
{
    beginRemoveRows(QModelIndex(), position, position+rows-1);

    for (int row = 0; row < rows; ++row)
    {
        m_waves.removeAt(position);
    }

    endRemoveRows();
    return true;
}

bool WaveListModel::setHeaderData ( int section, Qt::Orientation orientation, const QVariant & value, int role )
{
    return false;
}
